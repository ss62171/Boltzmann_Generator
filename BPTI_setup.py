#!/usr/bin/env python3.7
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import keras
import tensorflow as tf


# In[2]:


from IPython.display import SVG


# In[3]:


import mdtraj, nglview


# In[4]:


rcParams.update({'font.size': 16})


# In[5]:


# Switch AUTORELOAD ON. Disable this when in production mode!
# get_ipython().magic('load_ext autoreload')
# get_ipython().magic('autoreload 2')


# In[6]:


from deep_boltz.util import save_obj, load_obj


# BPTI
# -----

# In[7]:


from scipy.special import logsumexp


# In[8]:


import numpy as np
from matplotlib import rcParams
import keras
import tensorflow as tf

from deep_boltz.models.openmm import OpenMMEnergy
from deep_boltz import openmmutils
from deep_boltz.networks.invertible import EnergyInvNet
import mdtraj as md
from simtk import openmm, unit


# In[9]:


# setup BPTI
INTEGRATOR_ARGS = (300*unit.kelvin,
                   1.0/unit.picoseconds,
                    2.0*unit.femtoseconds)

data_sets=[]
pdb = openmm.app.PDBFile('./bpti/bpti/1187409s_pdb1.pdb')
forcefield = openmm.app.ForceField('amber99sbildn.xml', 'amber99_obc.xml')

system = forcefield.createSystem(pdb.topology,
        nonbondedMethod=openmm.app.CutoffNonPeriodic, nonbondedCutoff=1.0*unit.nanometers,
        constraints=None, rigidWater=True)

integrator = openmm.LangevinIntegrator(300*unit.kelvin, 1.0/unit.picoseconds,
            2.0*unit.femtoseconds)


# In[10]:


simulation = openmm.app.Simulation(pdb.topology, system, integrator)
simulation.context.setPositions(pdb.positions)
simulation.minimizeEnergy()


# In[11]:


def is_cartesian(atom):
    v1 = atom.residue.name == 'CYS' and atom.is_sidechain and atom.element.symbol is not 'H'
    v2 = atom.name in ['CA', 'C', 'N']
    return v1 or v2


# In[12]:


def mdtraj2Z(mdtraj_topology, cartesian=None):
    from deep_boltzmann.models.proteins import basis_Zs
    Z = []

    notIC = []
    if cartesian != None:
        notIC = mdtraj_topology.select(cartesian)

    for i,_ in enumerate(mdtraj_topology.residues):
        nterm = i==0
        cterm = (i + 1) == mdtraj_topology.n_residues

        resatoms = {a.name:a.index for a in _.atoms}
        resname = _.name
        for entry in basis_Zs[resname]:
            if resatoms[entry[0]] not in notIC:
                Z.append([resatoms[_e] for _e in entry])

        if nterm:
            # set two additional N-term protons
            if resatoms["H2"] not in notIC:
                Z.append([resatoms["H2"], resatoms["N"], resatoms["CA"], resatoms["H"] ])
            if resatoms["H3"] not in notIC:
                Z.append([resatoms["H3"], resatoms["N"], resatoms["CA"], resatoms["H2"] ])
        if cterm:
            # place OXT
            if resatoms["OXT"] not in notIC:
                Z.append([resatoms["OXT"], resatoms["C"], resatoms["CA"], resatoms["O"] ])
    if cartesian != None:
        return Z, notIC
    else:
        return Z


# In[26]:


bpti_omm_energy = OpenMMEnergy(system,
                               openmm.LangevinIntegrator,
                               unit.nanometers,
                               n_atoms=md.Topology().from_openmm(simulation.topology).n_atoms,
                               openmm_integrator_args=INTEGRATOR_ARGS
                              )

configuration = np.array(pdb.positions)
bpti_omm_energy._assign_openmm_positions(configuration)
simulation.reporters.append(openmm.app.PDBReporter('output.pdb', 10))
simulation.step(100)
mdtraj_topology = md.Topology().from_openmm(pdb.topology)

## Load Trajectories.
x = md.load('output.pdb')
x = x.xyz

## Split into Cartesian Set and Internal Coordinate Set.
cartesian_set = []
for atom in mdtraj_topology.atoms:
    if is_cartesian(atom):
        cartesian_set.append(atom.index)

cartesian_set = np.array(cartesian_set)

Z_, carts_ = mdtraj2Z(mdtraj_topology, cartesian="resname CYS and mass>2 and sidechain")
Z_ = np.array(Z_)
print(Z_.shape)


# In[27]:


data_dir = "./data/"


# In[28]:


#load training data for ML loss
sim_x = np.load(data_dir + "training_data.npy")
# Superimpose frames and reshuffle 
nframes = sim_x.shape[0]
dim = sim_x.shape[1]
# print(sim_x.shape)
sim_x_traj = mdtraj.Trajectory(sim_x.reshape((nframes, int(dim/3), 3)), mdtraj_topology)
sim_x_traj = sim_x_traj.superpose(sim_x_traj[0], atom_indices=mdtraj_topology.select('backbone'))
sim_x = sim_x_traj.xyz.reshape((nframes, -1))
np.random.shuffle(sim_x)

print('Data loaded\n')
print(sim_x.shape)


# In[17]:


def WhitenCord(X0, dims=None):
    print(X0.shape)
    if dims is None:
        dims = X0.shape[1]
    X0mean = X0.mean(axis = 0)
    print('X',X0mean.shape)
    X0meanfree = X0 - X0mean
    C = np.dot(X0meanfree.T, X0meanfree) / (X0meanfree.shape[0] - 1.0)
    print('C=',C.shape)
    eigval, eigvec = np.linalg.eigh(C)
    I = np.argsort(eigval)[::-1]
    I = I[:dims]
    eigval = eigval[I]
    eigvec = eigvec[:, I]
    std = np.sqrt(eigval)
    std = np.diag(1.0/std)
    WhitenMatrix = std@eigvec.T
    print(WhitenMatrix.shape, X0.shape)
    X0Whitened = (WhitenMatrix@X0.T).T
    X0Whitenedmeanfree = X0Whitened - X0Whitened.mean(axis = 0)
    C = np.dot(X0Whitenedmeanfree.T, X0Whitenedmeanfree) / (X0Whitenedmeanfree.shape[0] - 1.0)
    for i in range(C.shape[0]):
        print(C[i][i])
    return WhitenMatrix


# In[18]:


cart_indices = np.concatenate([[i*3, i*3+1, i*3+2] for i in cartesian_set])
# WhitenCord(sim_x[:,cart_indices], cart_indices.size - 6)


# In[37]:


# print(sim_x.shape[1])
from deep_boltz.networks.invertible import invnet


# In[25]:


# bg = invnet(sim_x.shape[1], 'R'*8, energy_model=bpti_omm_energy,
#             ic=Z_, ic_cart=cartesian_set, ic_norm=sim_x,
#             nl_layers=4, nl_hidden=[256, 128, 256], nl_activation='relu', nl_activation_scale='tanh')


# In[40]:


from deep_boltz.networks.training import MLTrainer


# In[41]:


def train_ML(bg, xtrain, epochs, batch_sizes):
    trainer_ML = MLTrainer(bg, lr=0.001)
    for batch_size in batch_sizes:
        trainer_ML.train(xtrain, epochs=epochs, batch_size=batch_size)


# In[28]:


# Perform ML training -- Three epochs with increasing batch sizes
# epochs_ML = 2000
# batch_sizes_ML = [128, 256, 512]
# train_ML(bg, sim_x, epochs_ML, batch_sizes_ML)
# bg.save('./BG_save_after_ML.pkl')

# print('ML training done\n')


# In[ ]:


bg = EnergyInvNet.load("./BG_save_after_ML.pkl", bpti_omm_energy)


# In[38]:


def torsion_tf(x1, x2, x3, x4, degrees=True):
    """Praxeolitic formula
    1 sqrt, 1 cross product"""
    b0 = -1.0*(x2 - x1)
    b1 = x3 - x2
    b2 = x4 - x3
    # normalize b1 so that it does not influence magnitude of vector
    # rejections that come next
    b1 /= tf.norm(b1, axis=2, keepdims=True)

    # vector rejections
    # v = projection of b0 onto plane perpendicular to b1
    #   = b0 minus component that aligns with b1
    # w = projection of b2 onto plane perpendicular to b1
    #   = b2 minus component that aligns with b1
    v = b0 - tf.reduce_sum(b0*b1, axis=2, keepdims=True) * b1
    w = b2 - tf.reduce_sum(b2*b1, axis=2, keepdims=True) * b1

    # angle between v and w in a plane is the torsion angle
    # v and w may not be normalized but that's fine since tan is y/x
    x = tf.reduce_sum(v*w, axis=2)
    b1xv = tf.cross(b1, v)
    y = tf.reduce_sum(b1xv*w, axis=2)
    if degrees:
        return np.float32(180.0 / np.pi) * tf.atan2(y, x)
    else:
        return tf.atan2(y, x)
    
class TorsionTICARC(object):
    def __init__(self, dim, torsion_idx, tica_mean, tica_eig, output_dims=2):
        self.torsion_idx = torsion_idx
        self.tica_mean = tf.constant(tica_mean.astype(np.float32))
        self.tica_eig = tf.constant(tica_eig.astype(np.float32))[:,:output_dims]
        self.atom_indices = np.arange(dim).reshape((-1, 3))
        self.n_torsions = torsion_idx.shape[0]
        self.dim = output_dims

    def __call__(self, x):
        #compute torsions
        torsions = torsion_tf(tf.gather(x, self.atom_indices[self.torsion_idx[:, 0]], axis=1),
                            tf.gather(x, self.atom_indices[self.torsion_idx[:, 1]], axis=1),
                            tf.gather(x, self.atom_indices[self.torsion_idx[:, 2]], axis=1),
                            tf.gather(x, self.atom_indices[self.torsion_idx[:, 3]], axis=1), degrees=False)
        #compute sines and cosines and stack and flatten
        torsion_cossin = tf.reshape(tf.stack([tf.cos(torsions), tf.sin(torsions)], axis=-1), 
                                    [-1, 2*self.n_torsions])
        torsion_cossin_mean_free = torsion_cossin - self.tica_mean
        if self.dim == 1:
            return tf.cast(tf.reshape(tf.matmul(torsion_cossin_mean_free, self.tica_eig), [-1]), tf.float32)
        else:
            return tf.cast(tf.reshape(tf.matmul(torsion_cossin_mean_free, self.tica_eig), [-1, self.dim]), tf.float32)


# In[39]:


# tica_data_folder = "./tica_data/"
# TicaRC = TorsionTICARC(bpti_omm_energy.dim, np.load(tica_data_folder+"/torsion_idx.npy"), 
#                           np.load(tica_data_folder+"/tica_mean.npy"), 
#                           np.load(tica_data_folder+"/tica_eig_intern.npy"), output_dims=2)


# In[40]:


saveconfig = {}
import sys
from deep_boltz.networks.training import FlexibleTrainer


# In[63]:


def train_KL(bg, xtrain, epochs, high_energies, w_KLs, stage=0, rc_func=None, rc_min=-1, 
             rc_max=6, w_RC=1., w_L2_angle=0., free_energy=''):
    trainers_KL_state3 = []
    for current_stage in range(stage, len(epochs)):
        print('-----------------------')
        print(high_energies[current_stage], w_KLs[current_stage])
        sys.stdout.flush()
        flextrainer = FlexibleTrainer(bg, lr=0.0001, batch_size=5000,
                                      high_energy=high_energies[current_stage], max_energy=1e20,
                                      w_KL=w_KLs[current_stage], w_ML=1, weigh_ML=False, w_RC=w_RC,
                                      rc_func=rc_func, rc_min=np.array(rc_min), rc_max=np.array(rc_max),
                                      w_L2_angle=w_L2_angle, 
                                      rc_dims=rc_func.dim)
        flextrainer.train(xtrain, epochs=epochs[current_stage])
        trainers_KL_state3.append(flextrainer)

        # Analyze
        samples_z = np.random.randn(10000, bg.dim)
        samples_x = bg.Tzx.predict(samples_z)
        samples_e = bg.energy_model.energy(samples_x)
        energy_violations = [np.count_nonzero(samples_e > E) for E in high_energies]
        print('Energy violations:')
        for E, V in zip(high_energies, energy_violations):
            print(V, '\t>\t', E)
        sys.stdout.flush()

        # SAVE
        if free_energy=='':
            bg.save('./BG_intermediate_model_saved.pkl')
        else:
            bg.save(free_energy)
        saveconfig = {}
        saveconfig['stage'] = current_stage
        #np.savez_compressed('config_save.npz', **saveconfig)
        print('Intermediate result saved')
        sys.stdout.flush()


# In[43]:


# Train KL
# saveconfig['stage'] = 0

# epochs_KL     = [  15,   15,   15,   15,   15,   15,  20,  20,  30, 50, 50, 300]
# high_energies = [1e10,  1e9,  1e8,  1e7,  1e6,  1e5,  1e5,  1e5,  1e4,  1e4,  1e3,  1e3]
# w_KLs         = [1e-12, 1e-6, 1e-5, 1e-4, 1e-3, 1e-3, 5e-3, 1e-3, 5e-3, 5e-2, 0.05, 0.05]
# # epochs_KL = [15]
# # high_energies = [1e10]
# # w_KLs = [1e-12]
# train_KL(bg, sim_x, epochs_KL, high_energies, w_KLs, saveconfig['stage'],
#                      rc_func=TicaRC, rc_min=[-1, -2.1], rc_max=[6, 1.75], w_RC=20.0, 
#          w_L2_angle=1.0)


# In[23]:


# print('StdZ = ', bg.std_z(sim_x[::100]))


# In[17]:


# bg = EnergyInvNet.load("./BG_intermediate_model_saved.pkl", bpti_omm_energy)


# In[18]:


# print('StdZ = ', bg.std_z(sim_x[::100]))


# In[30]:


# sim_x[::20,:]
# a = np.array([[1,2,3],[4,5,6],[7,8,9],[10,11,12], [1,2,3],[4,5,6],[7,8,9],[10,11,12]])
# a[::4,:]


# In[45]:


# xsamples = bg.transform_zx(np.random.randn(10000, bg.dim))
# print(xsamples.shape)
# xenergies = bpti_omm_energy.energy(xsamples)
# print(xenergies.min())


# In[44]:


# import matplotlib.pyplot as plt
# plt.hist(xenergies[xenergies<0],bins=128,normed=True,alpha=0.4);
# plt.hist(bpti_omm_energy.energy(sim_x[::20,:]), bins=128,normed=True,alpha=0.4);
# plt.savefig('PE_DIST.png')


# In[29]:


def setup_BPTI(pdb_file):
    INTEGRATOR_ARGS = (300*unit.kelvin,
                       1.0/unit.picoseconds,
                       2.0*unit.femtoseconds)
    pdb = openmm.app.PDBFile('./bpti/bpti/' + pdb_file)
    forcefield = openmm.app.ForceField('amber99sbildn.xml', 'amber99_obc.xml')

    system = forcefield.createSystem(pdb.topology,
            nonbondedMethod=openmm.app.CutoffNonPeriodic, nonbondedCutoff=1.0*unit.nanometers,
            constraints=None, rigidWater=True)

    integrator = openmm.LangevinIntegrator(300*unit.kelvin, 1.0/unit.picoseconds,
                2.0*unit.femtoseconds)

    simulation = openmm.app.Simulation(pdb.topology, system, integrator)
    simulation.context.setPositions(pdb.positions)
    simulation.minimizeEnergy()
    mdtraj_topology = md.Topology().from_openmm(pdb.topology)
    bpti_omm_energy = OpenMMEnergy(system,
                               openmm.LangevinIntegrator,
                               unit.nanometers,
                               n_atoms=md.Topology().from_openmm(simulation.topology).n_atoms,
                               openmm_integrator_args=INTEGRATOR_ARGS
                              )
    return simulation, bpti_omm_energy, mdtraj_topology


# In[30]:


free_energy_dir = "free_energy_data/"
pdb1, pdb2 = "1187409s_pdb2.pdb", "1187409s_pdb3.pdb"
sim1, bpti_energy1, md_top1 = setup_BPTI(pdb1)
sim2, bpti_energy2, md_top2 = setup_BPTI(pdb2)
import os


# In[53]:


def save_traj(simulation, traj_file):
    simulation.reporters.append(openmm.app.PDBReporter(free_energy_dir+ 'output.pdb', 10))
    simulation.step(10000)
    ## Load Trajectories.
    x = md.load(free_energy_dir + 'output.pdb')
    x = x.xyz
    os.remove(free_energy_dir + 'output.pdb')
    traj_data = np.array(x).reshape((x.shape[0], 892*3))
    np.save(free_energy_dir + traj_file, traj_data)


# In[54]:


traj1, traj2 = "train1.npy", "train2.npy"
save_traj(sim1, traj1)
save_traj(sim2, traj2)


# In[55]:


def load_data(train_file, md_top):
    #load training data for ML loss
    sim_x = np.load(free_energy_dir + train_file)
    # Superimpose frames and reshuffle 
    nframes = sim_x.shape[0]
    dim = sim_x.shape[1]
    sim_x_traj = mdtraj.Trajectory(sim_x.reshape((nframes, int(dim/3), 3)), md_top)
    sim_x_traj = sim_x_traj.superpose(sim_x_traj[0], atom_indices=mdtraj_topology.select('backbone'))
    sim_x = sim_x_traj.xyz.reshape((nframes, -1))
    np.random.shuffle(sim_x)

    print('Data loaded')
    print(sim_x.shape)
    return sim_x


# In[59]:


sim_x1, sim_x2 = load_data(traj1, md_top1), load_data(traj2, md_top2)
# bg1 = invnet(sim_x1.shape[1], 'R'*8, energy_model=bpti_energy1,
#             ic=Z_, ic_cart=cartesian_set, ic_norm=sim_x1,
#             nl_layers=4, nl_hidden=[256, 128, 256], nl_activation='relu', nl_activation_scale='tanh')
# bg2 = invnet(sim_x2.shape[1], 'R'*8, energy_model=bpti_energy2,
#             ic=Z_, ic_cart=cartesian_set, ic_norm=sim_x2,
#             nl_layers=4, nl_hidden=[256, 128, 256], nl_activation='relu', nl_activation_scale='tanh')


# In[60]:


# plt.hist(bpti_energy1.energy(sim_x1[::2,:]), bins=128,normed=True,alpha=0.4);


# In[61]:


# epochs_ML = 200
# batch_sizes_ML = [128, 256, 512]
# train_ML(bg1, sim_x1, epochs_ML, batch_sizes_ML)
# bg1.save(free_energy_dir + './BG_save_after_ML1.pkl')

# print('ML training done\n')


# In[62]:


# train_ML(bg2, sim_x2, epochs_ML, batch_sizes_ML)
# bg2.save(free_energy_dir + './BG_save_after_ML2.pkl')

# print('ML training done\n')


# In[64]:


print(free_energy_dir + "BG_intermediate_model_saved1.pkl")

def train_KL_free(bg, xtrain, epochs, high_energies, w_KLs, stage=0, free_energy=''):
    trainers_KL_state3 = []
    for current_stage in range(stage, len(epochs)):
        print('-----------------------')
        print(high_energies[current_stage], w_KLs[current_stage])
        sys.stdout.flush()
        flextrainer = FlexibleTrainer(bg, lr=0.0001, batch_size=5000,
                                      high_energy=high_energies[current_stage], max_energy=1e20,
                                      w_KL=w_KLs[current_stage], w_ML=1, weigh_ML=False)
        flextrainer.train(xtrain, epochs=epochs[current_stage])
        trainers_KL_state3.append(flextrainer)

        # Analyze
        samples_z = np.random.randn(10000, bg.dim)
        samples_x = bg.Tzx.predict(samples_z)
        samples_e = bg.energy_model.energy(samples_x)
        energy_violations = [np.count_nonzero(samples_e > E) for E in high_energies]
        print('Energy violations:')
        for E, V in zip(high_energies, energy_violations):
            print(V, '\t>\t', E)
        sys.stdout.flush()

        # SAVE
        if free_energy=='':
            bg.save('./BG_intermediate_model_saved.pkl')
        else:
            bg.save(free_energy)
        saveconfig = {}
        saveconfig['stage'] = current_stage
        #np.savez_compressed('config_save.npz', **saveconfig)
        print('Intermediate result saved')
        sys.stdout.flush()
# In[ ]:


# Train KL
saveconfig['stage'] = 0
bg1 = EnergyInvNet.load(free_energy_dir + "./BG_save_after_ML1.pkl", bpti_energy1)
bg2 = EnergyInvNet.load(free_energy_dir + "./BG_save_after_ML2.pkl", bpti_energy2)
epochs_KL     = [  15,   15,   15,   15,   15,   15,  20,  20,  30, 50, 50, 300]
high_energies = [1e10,  1e9,  1e8,  1e7,  1e6,  1e5,  1e5,  1e5,  1e4,  1e4,  1e3,  1e3]
w_KLs         = [1e-12, 1e-6, 1e-5, 1e-4, 1e-3, 1e-3, 5e-3, 1e-3, 5e-3, 5e-2, 0.05, 0.05]
train_KL_free(bg1, sim_x1, epochs_KL, high_energies, w_KLs, saveconfig['stage'], free_energy=free_energy_dir + "BG_intermediate_model_saved1.pkl")
train_KL_free(bg2, sim_x2, epochs_KL, high_energies, w_KLs, saveconfig['stage'], free_energy=free_energy_dir + "BG_intermediate_model_saved2.pkl")

